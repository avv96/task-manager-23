package ru.tsc.vinokurov.tm.repository;

import org.jetbrains.annotations.Nullable;
import lombok.NonNull;
import ru.tsc.vinokurov.tm.api.repository.IUserRepository;
import ru.tsc.vinokurov.tm.model.User;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Nullable
    @Override
    public User findOneById(@NonNull final String id) {
        return items.stream().filter(user -> id.equals(user.getId()))
                .findAny().orElse(null);
    }

    @Nullable
    @Override
    public User findOneByLogin(@NonNull final String login) {
        return items.stream().filter(user -> login.equals(user.getLogin()))
                .findAny().orElse(null);
    }

    @Nullable
    @Override
    public User findOneByEmail(@NonNull final String email) {
        return items.stream().filter(user -> email.equals(user.getEmail()))
                .findAny().orElse(null);
    }

    @Override
    public boolean existsByLogin(@NonNull final String login) {
        return findOneByLogin(login) != null;
    }

    @Override
    public boolean existsByEmail(@NonNull final String email) {
        return findOneByEmail(email) != null;
    }

}
