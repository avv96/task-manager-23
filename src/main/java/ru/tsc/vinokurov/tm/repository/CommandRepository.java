package ru.tsc.vinokurov.tm.repository;

import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.vinokurov.tm.api.repository.ICommandRepository;
import ru.tsc.vinokurov.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class CommandRepository implements ICommandRepository {
    @NonNull
    private final Map<String, AbstractCommand> mapByName = new LinkedHashMap<>();
    @NonNull
    private final Map<String, AbstractCommand> mapByArgument = new LinkedHashMap<>();

    @NonNull
    @Override
    public Collection<AbstractCommand> getTerminalCommands() {
        return mapByName.values();
    }

    @NonNull
    @Override
    public Collection<AbstractCommand> getArgumentCommands() {
        return mapByArgument.values();
    }

    @Override
    public void add(@NotNull @NonNull AbstractCommand command) {
        final String name = command.getName();
        if (!StringUtils.isEmpty(name)) mapByName.put(name, command);
        final String argument = command.getArgument();
        if (!StringUtils.isEmpty(argument)) mapByArgument.put(argument, command);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByName(@NonNull final String name) {
        return mapByName.get(name);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByArgument(@NonNull final String argument) {
        return mapByArgument.get(argument);
    }

}
