package ru.tsc.vinokurov.tm.repository;

import lombok.NonNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.vinokurov.tm.api.repository.IRepository;
import ru.tsc.vinokurov.tm.model.AbstractModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

//
public class AbstractRepository<M extends AbstractModel> implements IRepository<M> {
    @NonNull
    protected final List<M> items = new ArrayList<>();

    @NonNull
    @Override
    public List<M> findAll() {
        return items;
    }

    @NonNull
    @Override
    public List<M> findAll(@NonNull final Comparator comparator) {
        @NonNull final List<M> result = new ArrayList<>(items);
        result.sort(comparator);
        return result;
    }

    @Nullable
    @Override
    public M add(@NonNull final M item) {
        items.add(item);
        return item;
    }

    @Nullable
    @Override
    public M remove(@NonNull final M item) {
        items.remove(item);
        return item;
    }

    @Override
    public void removeAll(@NonNull final Collection<M> collection) {
        items.removeAll(collection);
    }

    @Override
    public void clear() {
        items.clear();
    }

    @Nullable
    @Override
    public M findOneByIndex(@NonNull final Integer index) {
        return items.get(index);
    }

    @Nullable
    @Override
    public M findOneById(@NonNull final String id) {
        return items.stream().filter(project -> id.equals(project.getId())).findAny().orElse(null);
    }

    @Nullable
    @Override
    public M removeById(@NonNull String id) {
        final M item = findOneById(id);
        if (item == null) return null;
        items.remove(item);
        return item;
    }

    @Nullable
    @Override
    public M removeByIndex(@NonNull Integer index) {
        final M item = findOneByIndex(index);
        if (item == null) return null;
        items.remove(item);
        return item;
    }

    @Override
    public int size() {
        return items.size();
    }

}
