package ru.tsc.vinokurov.tm.api.service;

import lombok.NonNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.vinokurov.tm.enumerated.Role;
import ru.tsc.vinokurov.tm.model.User;

public interface IAuthService {

    void login(@Nullable String login, @Nullable String password);

    void logout();

    @NonNull
    User register(@Nullable String login, @Nullable String password, @Nullable String email);

    @Nullable
    User getUser();

    @NonNull
    String getUserId();

    boolean isAuth();

    void checkRoles(@Nullable Role[] roles);

}
