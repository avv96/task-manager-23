package ru.tsc.vinokurov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import lombok.NonNull;
import ru.tsc.vinokurov.tm.api.repository.IUserOwnedRepository;
import ru.tsc.vinokurov.tm.enumerated.Sort;
import ru.tsc.vinokurov.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M>, IService<M> {
    @NonNull
    M removeById(@NonNull String userId, @NonNull String id);

    @NonNull
    M removeByIndex(@NonNull String userId, @NonNull Integer index);

    @Nullable
    List<M> findAll(@NonNull String userId, @NonNull Sort sort);

}
