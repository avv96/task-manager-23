package ru.tsc.vinokurov.tm.api.repository;

import lombok.NonNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.vinokurov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {
    @NonNull
    Task create(@NonNull String userId, @NonNull String name);

    @NonNull
    Task create(@NonNull String userId, @NonNull String name, @NonNull String description);

    @NonNull
    List<Task> findAllByProjectId(@NonNull String userId, @NonNull String projectId);

    boolean existsById(@NonNull String userId, @NonNull String id);

}
