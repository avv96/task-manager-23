package ru.tsc.vinokurov.tm.api.service;

import lombok.NonNull;

public interface IServiceLocator {
    @NonNull
    ICommandService getCommandService();

    @NonNull
    ITaskService getTaskService();

    @NonNull
    IProjectService getProjectService();

    @NonNull
    IProjectTaskService getProjectTaskService();

    @NonNull
    ILoggerService getLoggerService();

    @NonNull
    IUserService getUserService();

    @NonNull
    IAuthService getAuthService();
}
