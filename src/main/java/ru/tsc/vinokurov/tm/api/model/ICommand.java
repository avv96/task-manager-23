package ru.tsc.vinokurov.tm.api.model;

import lombok.NonNull;

public interface ICommand {
    @NonNull
    String getName();

    @NonNull
    String getDescription();

    @NonNull
    String getArgument();

}
