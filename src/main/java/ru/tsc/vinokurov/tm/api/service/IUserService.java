package ru.tsc.vinokurov.tm.api.service;

import lombok.NonNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.vinokurov.tm.enumerated.Role;
import ru.tsc.vinokurov.tm.model.User;

public interface IUserService extends IService<User> {
    @Nullable
    User findOneByLogin(@Nullable String login);

    @Nullable
    User findOneByEmail(@Nullable String email);

    @Nullable
    User removeByLogin(@Nullable String login);

    @NonNull
    User create(@Nullable String login, @Nullable String password);

    @NonNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NonNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @NonNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email, Role role);

    @NonNull
    User setPassword(@Nullable String userId, @Nullable String password);

    @NonNull
    User updateUser(@Nullable String userId, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

}
