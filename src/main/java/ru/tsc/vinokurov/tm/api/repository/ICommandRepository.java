package ru.tsc.vinokurov.tm.api.repository;

import lombok.NonNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.vinokurov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {
    @NonNull
    Collection<AbstractCommand> getTerminalCommands();

    @NonNull
    Collection<AbstractCommand> getArgumentCommands();

    void add(@NonNull AbstractCommand command);

    @Nullable
    AbstractCommand getCommandByName(@NonNull String name);

    @Nullable
    AbstractCommand getCommandByArgument(@NonNull String argument);

}
