package ru.tsc.vinokurov.tm.api.model;

import lombok.NonNull;

import java.util.Date;

public interface IHasDateBegin {
    @NonNull
    Date getDateBegin();

    void setDateBegin(@NonNull Date dateBegin);

}
