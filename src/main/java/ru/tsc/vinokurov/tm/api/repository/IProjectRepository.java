package ru.tsc.vinokurov.tm.api.repository;

import lombok.NonNull;
import ru.tsc.vinokurov.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {
    @NonNull
    Project create(@NonNull String userId, @NonNull String name);

    @NonNull
    Project create(@NonNull String userId, @NonNull String name, @NonNull String description);

    boolean existsById(@NonNull String userId, @NonNull String id);

}
