package ru.tsc.vinokurov.tm.api.repository;

import lombok.NonNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.vinokurov.tm.model.User;

public interface IUserRepository extends IRepository<User> {
    @Nullable
    User findOneByLogin(@NonNull String login);

    @Nullable
    User findOneByEmail(@NonNull String email);

    boolean existsByLogin(@NonNull String login);

    boolean existsByEmail(@NonNull String email);

}
