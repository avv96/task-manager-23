package ru.tsc.vinokurov.tm.api.repository;


import lombok.NonNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.vinokurov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {
    @Nullable
    List<M> findAll(@NonNull String userId);

    @Nullable
    List<M> findAll(@NonNull String userId, @NonNull Comparator comparator);

    @Nullable
    M add(@NonNull String userId, @NonNull M item);

    @Nullable
    M remove(@NonNull String userId, @NonNull M item);

    void clear(@NonNull String userId);

    @Nullable
    M findOneByIndex(@NonNull String userId, @NonNull Integer index);

    @Nullable
    M findOneById(@NonNull String userId, String id);

    int size(String userId);

}
