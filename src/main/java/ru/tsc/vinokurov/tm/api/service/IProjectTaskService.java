package ru.tsc.vinokurov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.tsc.vinokurov.tm.model.Project;

public interface IProjectTaskService {

    void bindTaskToProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    void unbindTaskFromProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    void removeProject(@Nullable String userId, @Nullable String projectId);

    void removeProject(@Nullable String userId, @Nullable Project project);
}
