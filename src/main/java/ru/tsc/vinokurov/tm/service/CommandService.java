package ru.tsc.vinokurov.tm.service;

import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.Nullable;
import ru.tsc.vinokurov.tm.api.repository.ICommandRepository;
import ru.tsc.vinokurov.tm.api.service.ICommandService;
import ru.tsc.vinokurov.tm.command.AbstractCommand;
import ru.tsc.vinokurov.tm.exception.field.NameEmptyException;
import ru.tsc.vinokurov.tm.exception.system.ArgumentNotSupportedException;
import ru.tsc.vinokurov.tm.exception.system.CommandNotSupportedException;

import java.util.Collection;
import java.util.Optional;

public class CommandService implements ICommandService {
    @NonNull
    private final ICommandRepository commandRepository;

    public CommandService(@NonNull ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @NonNull
    @Override
    public Collection<AbstractCommand> getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

    @NonNull
    @Override
    public Collection<AbstractCommand> getArgumentCommands() {
        return commandRepository.getArgumentCommands();
    }

    @Override
    public void add(@Nullable AbstractCommand command) {
        Optional.ofNullable(command).orElseThrow(CommandNotSupportedException::new);
        commandRepository.add(command);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByName(@Nullable String name) {
        if (StringUtils.isEmpty(name)) throw new NameEmptyException();
        return commandRepository.getCommandByName(name);
    }
    @Nullable
    @Override
    public AbstractCommand getCommandByArgument(@Nullable String argument) {
        if (StringUtils.isEmpty(argument)) throw new ArgumentNotSupportedException();
        return commandRepository.getCommandByArgument(argument);
    }

}
