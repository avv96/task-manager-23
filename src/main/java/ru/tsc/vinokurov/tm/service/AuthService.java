package ru.tsc.vinokurov.tm.service;

import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.Nullable;
import ru.tsc.vinokurov.tm.api.service.IAuthService;
import ru.tsc.vinokurov.tm.api.service.IUserService;
import ru.tsc.vinokurov.tm.enumerated.Role;
import ru.tsc.vinokurov.tm.exception.field.LoginEmptyException;
import ru.tsc.vinokurov.tm.exception.field.PasswordEmptyException;
import ru.tsc.vinokurov.tm.exception.system.AccessDeniedException;
import ru.tsc.vinokurov.tm.exception.system.PermissionException;
import ru.tsc.vinokurov.tm.model.User;
import ru.tsc.vinokurov.tm.util.HashUtil;

import java.util.Arrays;
import java.util.Optional;

public class AuthService implements IAuthService {
    @NonNull
    private final IUserService userService;
    @Nullable
    private String userId;

    public AuthService(@NonNull final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public void login(@Nullable final String login, @Nullable final String password) {
        if (StringUtils.isEmpty(login)) throw new LoginEmptyException();
        if (StringUtils.isEmpty(password)) throw new PasswordEmptyException();
        final User user = Optional.ofNullable(userService.findOneByLogin(login)).orElseThrow(AccessDeniedException::new);
        if (user.isLocked()) throw new AccessDeniedException();
        if (!user.getPasswordHash().equals(HashUtil.salt(password)))
            throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @NonNull
    @Override
    public User register(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        return userService.create(login, password, email);
    }

    @Nullable
    @Override
    public User getUser() {
        return userService.findOneById(getUserId());
    }

    @NonNull
    @Override
    public String getUserId() {
        if (userId == null) throw new PermissionException();
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    public void checkRoles(@Nullable final Role[] roles) {
        if (roles == null) return;
        final User user = getUser();
        final Role userRole = Optional.ofNullable(user.getRole()).orElseThrow(PermissionException::new);
        final boolean matchUserRole = Arrays.asList(roles).contains(userRole);
        if (!matchUserRole) throw new PermissionException();
    }

}
