package ru.tsc.vinokurov.tm.service;

import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.Nullable;
import ru.tsc.vinokurov.tm.api.repository.IUserOwnedRepository;
import ru.tsc.vinokurov.tm.api.service.IUserOwnedService;
import ru.tsc.vinokurov.tm.enumerated.Sort;
import ru.tsc.vinokurov.tm.exception.entity.ItemNotFoundException;
import ru.tsc.vinokurov.tm.exception.field.IdEmptyException;
import ru.tsc.vinokurov.tm.exception.field.IndexIncorrectException;
import ru.tsc.vinokurov.tm.exception.field.UserIdEmptyException;
import ru.tsc.vinokurov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>> extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(@NonNull final R repository) {
        super(repository);
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final String userId) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        return repository.findAll(userId);
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Comparator comparator) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        if (comparator == null) findAll(userId);
        return repository.findAll(userId, comparator);
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        return repository.findAll(userId, sort.getComparator());
    }

    @Nullable
    @Override
    public M add(@Nullable final String userId, @Nullable final M item) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        if (item == null) throw new ItemNotFoundException();
        return repository.add(userId, item);
    }

    @Nullable
    @Override
    public M remove(@Nullable final String userId, @Nullable final M item) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        return repository.remove(userId, item);
    }

    @NonNull
    @Override
    public M removeById(@Nullable final String userId, @Nullable final String id) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        final M item = Optional.ofNullable(findOneById(id)).orElseThrow(ItemNotFoundException::new);
        return repository.remove(userId, item);
    }

    @NonNull
    @Override
    public M removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        final M item = Optional.ofNullable(findOneByIndex(index)).orElseThrow(ItemNotFoundException::new);
        return repository.remove(userId, item);
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        repository.clear(userId);
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= repository.size()) throw new IndexIncorrectException();
        return repository.findOneByIndex(userId, index);
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String userId, @Nullable final String id) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        if (StringUtils.isEmpty(id)) throw new IdEmptyException();
        return repository.findOneById(userId, id);
    }

    @Override
    public int size(final String userId) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        return repository.size(userId);
    }

}
