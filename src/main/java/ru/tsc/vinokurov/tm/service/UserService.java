package ru.tsc.vinokurov.tm.service;

import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.Nullable;
import ru.tsc.vinokurov.tm.api.repository.IProjectRepository;
import ru.tsc.vinokurov.tm.api.repository.ITaskRepository;
import ru.tsc.vinokurov.tm.api.repository.IUserRepository;
import ru.tsc.vinokurov.tm.api.service.IUserService;
import ru.tsc.vinokurov.tm.enumerated.Role;
import ru.tsc.vinokurov.tm.exception.entity.UserExistsByEmailException;
import ru.tsc.vinokurov.tm.exception.entity.UserExistsByLoginException;
import ru.tsc.vinokurov.tm.exception.entity.UserNotFoundException;
import ru.tsc.vinokurov.tm.exception.field.*;
import ru.tsc.vinokurov.tm.model.User;
import ru.tsc.vinokurov.tm.util.HashUtil;

import java.util.Optional;

public class UserService extends AbstractService<User, IUserRepository> implements IUserService {
    @NonNull
    private ITaskRepository taskRepository;
    @NonNull
    private IProjectRepository projectRepository;

    public UserService(
            @NonNull final IUserRepository userRepository,
            @NonNull final ITaskRepository taskRepository,
            @NonNull final IProjectRepository projectRepository
    ) {
        super(userRepository);
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Nullable
    @Override
    public User findOneById(@Nullable final String id) {
        if (StringUtils.isEmpty(id)) throw new UserIdEmptyException();
        return repository.findOneById(id);
    }

    @Nullable
    @Override
    public User findOneByLogin(@Nullable final String login) {
        if (StringUtils.isEmpty(login)) throw new LoginEmptyException();
        return repository.findOneByLogin(login);
    }

    @NonNull
    @Override
    public User remove(@Nullable final User user) {
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        final User userToRemove = super.remove(user);
        if (userToRemove == null) return null;
        final String userId = userToRemove.getId();
        taskRepository.clear(userId);
        projectRepository.clear(userId);
        return userToRemove;
    }

    @Nullable
    @Override
    public User findOneByEmail(@Nullable final String email) {
        if (StringUtils.isEmpty(email)) throw new EmailEmptyException();
        return repository.findOneByEmail(email);
    }

    @NonNull
    @Override
    public User removeByLogin(@Nullable final String login) {
        if (StringUtils.isEmpty(login)) throw new EmailEmptyException();
        final User user = Optional.ofNullable(findOneByLogin(login)).orElseThrow(UserNotFoundException::new);
        return remove(user);
    }

    @NonNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password) {
        if (StringUtils.isEmpty(login)) throw new LoginEmptyException();
        if (StringUtils.isEmpty(password)) throw new PasswordEmptyException();
        if (repository.existsByLogin(login)) throw new UserExistsByLoginException();
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt((password)));
        return repository.add(user);
    }

    @NonNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (StringUtils.isEmpty(email)) throw new EmailEmptyException();
        if (repository.existsByEmail(email)) throw new UserExistsByEmailException();
        final User user = Optional.ofNullable(create(login, password)).orElseThrow(UserNotFoundException::new);
        user.setEmail(email);
        return repository.add(user);
    }

    @NonNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (role == null) throw new RoleEmptyException();
        final User user = Optional.ofNullable(create(login, password)).orElseThrow(UserNotFoundException::new);
        user.setRole(role);
        return repository.add(user);
    }

    @NonNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email, @Nullable final Role role) {
        Optional.ofNullable(role).orElseThrow(RoleEmptyException::new);
        final User user = Optional.ofNullable(create(login, password, email)).orElseThrow(UserNotFoundException::new);
        user.setRole(role);
        return repository.add(user);
    }

    @NonNull
    @Override
    public User setPassword(@Nullable final String userId, @Nullable final String password) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        if (StringUtils.isEmpty(password)) throw new PasswordEmptyException();
        final User user = Optional.ofNullable(findOneById(userId)).orElseThrow(UserNotFoundException::new);
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    @NonNull
    @Override
    public User updateUser(
            @Nullable final String userId, @Nullable final String firstName,
            @Nullable final String lastName, @Nullable final String middleName
    ) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        final User user = Optional.ofNullable(findOneById(userId)).orElseThrow(UserNotFoundException::new);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) {
        if (StringUtils.isEmpty(login)) throw new LoginEmptyException();
        final User user = Optional.ofNullable(findOneByLogin(login)).orElseThrow(UserNotFoundException::new);
        user.setLocked(true);
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        if (StringUtils.isEmpty(login)) throw new LoginEmptyException();
        final User user = Optional.ofNullable(findOneByLogin(login)).orElseThrow(UserNotFoundException::new);
        user.setLocked(false);
    }
}
