package ru.tsc.vinokurov.tm.service;

import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.Nullable;
import ru.tsc.vinokurov.tm.api.service.ILoggerService;

import java.io.IOException;
import java.util.logging.*;


public class LoggerService implements ILoggerService {
    @NonNull
    public static final String CONFIG_FILE = "/logger.properties";
    @NonNull
    public static final String COMMANDS = "COMMANDS";
    @NonNull
    public static final String COMMANDS_FILE = "./commands.xml";
    @NonNull
    public static final String ERRORS = "ERRORS";
    @NonNull
    public static final String ERROR_FILE = "./errors.xml";
    @NonNull
    public static final String MESSAGES = "MESSAGES";
    @NonNull
    public static final String MESSAGES_FILE = "./messages.xml";
    @NonNull
    private final LogManager manager = LogManager.getLogManager();
    @NonNull
    private final Logger root = Logger.getLogger("");
    @NonNull
    private final Logger commands = Logger.getLogger(COMMANDS);
    @NonNull
    private final Logger errors = Logger.getLogger(ERRORS);
    @NonNull
    private final Logger messages = Logger.getLogger(MESSAGES);
    @NonNull
    private final ConsoleHandler consoleHandler = getConsoleHandler();

    {
        init();
        registry(commands, COMMANDS_FILE, false);
        registry(errors, ERROR_FILE, true);
        registry(messages, MESSAGES_FILE, true);
    }

    private void init() {
        try {
            manager.readConfiguration(LoggerService.class.getResourceAsStream(CONFIG_FILE));
        } catch (final IOException e) {
            root.severe(e.getMessage());
        }
    }

    private ConsoleHandler getConsoleHandler() {
        final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord logRecord) {
                return logRecord.getMessage() + "\n";
            }
        });
        return handler;
    }

    private void registry(@NonNull final Logger logger, @NonNull final String fileName, final boolean isConsole) {
        try {
            if (isConsole) logger.addHandler(consoleHandler);
            logger.setUseParentHandlers(false);
            logger.addHandler(new FileHandler(fileName));
        } catch (IOException e) {
            root.severe(e.getMessage());
        }
    }

    @Override
    public void info(@Nullable String message) {
        if (StringUtils.isEmpty(message)) return;
        messages.info(message);
    }

    @Override
    public void command(@Nullable String message) {
        if (StringUtils.isEmpty(message)) return;
        commands.info(message);
    }

    @Override
    public void error(@Nullable Exception e) {
        if (e == null) return;
        errors.log(Level.SEVERE, e.getMessage(), e);
    }

    @Override
    public void debug(@Nullable String message) {
        if (StringUtils.isEmpty(message)) return;
        messages.fine(message);
    }

}
