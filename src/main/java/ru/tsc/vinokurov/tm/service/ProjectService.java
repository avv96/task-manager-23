package ru.tsc.vinokurov.tm.service;

import org.jetbrains.annotations.Nullable;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import ru.tsc.vinokurov.tm.api.repository.IProjectRepository;
import ru.tsc.vinokurov.tm.api.service.IProjectService;
import ru.tsc.vinokurov.tm.enumerated.Status;
import ru.tsc.vinokurov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.vinokurov.tm.exception.field.*;
import ru.tsc.vinokurov.tm.model.Project;

import java.util.Date;
import java.util.Optional;

public class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(@NonNull final IProjectRepository repository) {
        super(repository);
    }

    @NonNull
    @Override
    public Project create(@Nullable final String userId, @Nullable final String name) {
        if (StringUtils.isEmpty(name)) throw new NameEmptyException();
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        return repository.create(userId, name);
    }

    @NonNull
    @Override
    public Project create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        if (StringUtils.isEmpty(name)) throw new NameEmptyException();
        if (StringUtils.isEmpty(description)) throw new DescriptionEmptyException();
        return repository.create(userId, name, description);
    }

    @NonNull
    @Override
    public Project create(@Nullable final String userId, @Nullable final String name, @Nullable final String description, @Nullable final Date dateBegin, @Nullable final Date dateEnd) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        if (StringUtils.isEmpty(name)) throw new NameEmptyException();
        Optional.ofNullable(description).orElseThrow(DescriptionEmptyException::new);
        Optional.ofNullable(dateBegin).orElseThrow(DateBeginEmptyException::new);
        Optional.ofNullable(dateEnd).orElseThrow(DateEndEmptyException::new);
        @NonNull final Project project = Optional.ofNullable(create(userId, name, description))
                .orElseThrow(ProjectNotFoundException::new);
        project.setDateBegin(dateBegin);
        project.setDateEnd(dateEnd);
        return project;
    }

    @NonNull
    @Override
    public Project updateById(@Nullable final String userId, @Nullable final String id, final String name, @Nullable final String description) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        if (StringUtils.isEmpty(id)) throw new ProjectIdEmptyException();
        if (StringUtils.isEmpty(name)) throw new NameEmptyException();
        @NonNull final Project project = Optional.ofNullable(findOneById(userId, id))
                .orElseThrow(ProjectNotFoundException::new);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @NonNull
    @Override
    public Project updateByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final String name, @Nullable final String description) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        if (StringUtils.isEmpty(name)) throw new NameEmptyException();
        Optional.ofNullable(index).orElseThrow(IndexIncorrectException::new);
        Optional.ofNullable(description).orElseThrow(DescriptionEmptyException::new);
        @NonNull final Project project = Optional.ofNullable(findOneByIndex(userId, index))
                .orElseThrow(ProjectNotFoundException::new);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @NonNull
    @Override
    public Project changeStatusByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final Status status) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        Optional.ofNullable(status).orElseThrow(StatusEmptyException::new);
        @NonNull final Project project = Optional.ofNullable(findOneByIndex(userId, index))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(status);
        return project;
    }

    @NonNull
    @Override
    public Project changeStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        if (StringUtils.isEmpty(id)) throw new ProjectIdEmptyException();
        Optional.ofNullable(status).orElseThrow(StatusEmptyException::new);
        @NonNull final Project project = Optional.ofNullable(findOneById(userId, id))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(status);
        return project;
    }

}
