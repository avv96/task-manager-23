package ru.tsc.vinokurov.tm.service;

import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.Nullable;
import ru.tsc.vinokurov.tm.api.repository.ITaskRepository;
import ru.tsc.vinokurov.tm.api.service.ITaskService;
import ru.tsc.vinokurov.tm.enumerated.Status;
import ru.tsc.vinokurov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.vinokurov.tm.exception.field.*;
import ru.tsc.vinokurov.tm.model.Task;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(@NonNull ITaskRepository taskRepository) {
        super(taskRepository);
    }

    @NonNull
    @Override
    public Task create(@Nullable final String userId, @Nullable final String name) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        if (StringUtils.isEmpty(name)) throw new NameEmptyException();
        return repository.create(userId, name);
    }

    @NonNull
    @Override
    public Task create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        if (StringUtils.isEmpty(name)) throw new NameEmptyException();
        if (StringUtils.isEmpty(description)) throw new DescriptionEmptyException();
        return repository.create(userId, name, description);
    }

    @NonNull
    @Override
    public Task create(@Nullable final String userId, @Nullable final String name, @Nullable final String description, @Nullable final Date dateBegin, @Nullable final Date dateEnd) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        if (StringUtils.isEmpty(name)) throw new NameEmptyException();
        if (StringUtils.isEmpty(description)) throw new DescriptionEmptyException();
        Optional.ofNullable(dateBegin).orElseThrow(DateBeginEmptyException::new);
        Optional.ofNullable(dateEnd).orElseThrow(DateEndEmptyException::new);
        @NonNull final Task task = Optional.ofNullable(create(userId, name, description))
                .orElseThrow(TaskNotFoundException::new);
        task.setDateBegin(dateBegin);
        task.setDateEnd(dateEnd);
        return task;
    }

    @NonNull
    @Override
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        if (StringUtils.isEmpty(projectId)) return Collections.emptyList();
        return repository.findAllByProjectId(userId, projectId);
    }

    @NonNull
    @Override
    public Task updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        if (StringUtils.isEmpty(id)) throw new TaskIdEmptyException();
        if (StringUtils.isEmpty(name)) throw new NameEmptyException();
        if (StringUtils.isEmpty(description)) throw new DescriptionEmptyException();
        @NonNull final Task task = Optional.ofNullable(findOneById(userId, id))
                .orElseThrow(TaskNotFoundException::new);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @NonNull
    @Override
    public Task updateByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final String name, @Nullable final String description) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        if (StringUtils.isEmpty(name)) throw new NameEmptyException();
        Optional.ofNullable(index).orElseThrow(IndexIncorrectException::new);
        if (StringUtils.isEmpty(description)) throw new DescriptionEmptyException();
        @NonNull final Task task = Optional.ofNullable(findOneByIndex(userId, index))
                .orElseThrow(TaskNotFoundException::new);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @NonNull
    @Override
    public Task changeStatusByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final Status status) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        Optional.ofNullable(status).orElseThrow(StatusEmptyException::new);
        Optional.ofNullable(index).orElseThrow(IndexIncorrectException::new);
        @NonNull final Task task = Optional.ofNullable(findOneByIndex(userId, index))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(status);
        return task;
    }

    @NonNull
    @Override
    public Task changeStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        if (StringUtils.isEmpty(id)) throw new TaskIdEmptyException();
        Optional.ofNullable(status).orElseThrow(StatusEmptyException::new);
        @NonNull final Task task = Optional.ofNullable(findOneById(userId, id))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(status);
        return task;
    }

}
