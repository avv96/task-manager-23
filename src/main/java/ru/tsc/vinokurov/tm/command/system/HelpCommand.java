package ru.tsc.vinokurov.tm.command.system;

import lombok.NonNull;
import ru.tsc.vinokurov.tm.api.model.ICommand;
import ru.tsc.vinokurov.tm.command.AbstractCommand;

import java.util.Collection;

public final class HelpCommand extends AbstractSystemCommand {

    @NonNull
    public static final String NAME = "help";

    @NonNull
    public static final String DESCRIPTION = "Show terminal commands.";

    @NonNull
    public static final String ARGUMENT = "-h";

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NonNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        @NonNull final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final ICommand command : commands) System.out.println(command);
    }

}
