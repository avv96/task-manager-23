package ru.tsc.vinokurov.tm.command.project;

import lombok.NonNull;
import ru.tsc.vinokurov.tm.model.Project;
import ru.tsc.vinokurov.tm.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @NonNull
    public static final String NAME = "project-remove-by-index";

    @NonNull
    public static final String DESCRIPTION = "Remove project by index.";

    public static final String ARGUMENT = null;

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NonNull final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = getProjectService().findOneByIndex(index);
        @NonNull final String userId = getUserId();
        getProjectTaskService().removeProject(userId, project);
    }

}
