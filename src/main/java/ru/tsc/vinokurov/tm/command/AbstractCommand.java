package ru.tsc.vinokurov.tm.command;

import lombok.NonNull;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import ru.tsc.vinokurov.tm.api.model.ICommand;
import ru.tsc.vinokurov.tm.api.service.IAuthService;
import ru.tsc.vinokurov.tm.api.service.IServiceLocator;
import ru.tsc.vinokurov.tm.enumerated.Role;

public abstract class AbstractCommand implements ICommand {

    @Setter
    protected IServiceLocator serviceLocator;

    public abstract void execute();

    public abstract Role[] getRoles();

    @NonNull
    protected IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    public String getUserId() {
        return getAuthService().getUserId();
    }

    @Override
    public String toString() {
        final String name = getName();
        final String description = getDescription();
        final String argument = getArgument();
        @NonNull String result = "";
        if (StringUtils.isNotEmpty(name)) result += name + " : ";
        if (StringUtils.isNotEmpty(argument)) result += argument + " : ";
        if (StringUtils.isNotEmpty(description)) result += description;
        return result;
    }

}
