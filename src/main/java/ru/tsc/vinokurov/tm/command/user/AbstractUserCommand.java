package ru.tsc.vinokurov.tm.command.user;

import lombok.NonNull;
import ru.tsc.vinokurov.tm.api.service.IUserService;
import ru.tsc.vinokurov.tm.command.AbstractCommand;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NonNull
    protected IUserService getUserService() {
        return serviceLocator.getUserService();
    }

}
