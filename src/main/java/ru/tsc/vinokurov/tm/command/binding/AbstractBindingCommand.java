package ru.tsc.vinokurov.tm.command.binding;

import lombok.NonNull;
import ru.tsc.vinokurov.tm.api.service.IProjectTaskService;
import ru.tsc.vinokurov.tm.command.AbstractCommand;
import ru.tsc.vinokurov.tm.enumerated.Role;

public abstract class AbstractBindingCommand extends AbstractCommand {

    @NonNull
    protected IProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }

    @NonNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
