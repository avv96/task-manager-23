package ru.tsc.vinokurov.tm.command.project;

import lombok.NonNull;
import ru.tsc.vinokurov.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @NonNull
    public static final String NAME = "project-remove-by-id";

    @NonNull
    public static final String DESCRIPTION = "Remove project by id.";

    public static final String ARGUMENT = null;

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NonNull final String projectId = TerminalUtil.nextLine();
        @NonNull final String userId = getUserId();
        getProjectTaskService().removeProject(userId, projectId);
    }

}
