package ru.tsc.vinokurov.tm.command.user;

import lombok.NonNull;
import ru.tsc.vinokurov.tm.enumerated.Role;
import ru.tsc.vinokurov.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @NonNull
    public static final String NAME = "user-change-password";

    @NonNull
    public static final String DESCRIPTION = "Change user password.";

    public static final String ARGUMENT = null;

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NonNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE USER PASSWORD]");
        final String userId = getAuthService().getUserId();
        System.out.println("ENTER NEW PASSWORD:");
        @NonNull final String password = TerminalUtil.nextLine();
        getUserService().setPassword(userId, password);
    }

    @NonNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
