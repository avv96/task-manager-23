package ru.tsc.vinokurov.tm.command.task;

import lombok.NonNull;
import ru.tsc.vinokurov.tm.util.TerminalUtil;

public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @NonNull
    public static final String NAME = "task-remove-by-index";

    @NonNull
    public static final String DESCRIPTION = "Remove task by index.";

    public static final String ARGUMENT = null;

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NonNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NonNull final String userId = getUserId();
        getTaskService().removeByIndex(userId, index);
    }

}
