package ru.tsc.vinokurov.tm.command.task;

import lombok.NonNull;
import ru.tsc.vinokurov.tm.model.Task;
import ru.tsc.vinokurov.tm.util.TerminalUtil;

public final class TaskShowByIdCommand extends AbstractTaskCommand {

    @NonNull
    public static final String NAME = "task-show-by-id";

    @NonNull
    public static final String DESCRIPTION = "Show task by id.";

    public static final String ARGUMENT = null;

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");
        @NonNull final String id = TerminalUtil.nextLine();
        @NonNull final String userId = getUserId();
        final Task task = getTaskService().findOneById(userId, id);
        showTask(task);
    }

}
