package ru.tsc.vinokurov.tm.command.user;

import lombok.NonNull;
import ru.tsc.vinokurov.tm.enumerated.Role;

public final class UserLogoutCommand extends AbstractUserCommand {

    @NonNull
    public static final String NAME = "logout";

    @NonNull
    public static final String DESCRIPTION = "Logout user.";

    public static final String ARGUMENT = null;

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        getAuthService().logout();
    }

    @NonNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
