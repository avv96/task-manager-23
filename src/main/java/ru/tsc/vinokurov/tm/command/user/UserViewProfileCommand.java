package ru.tsc.vinokurov.tm.command.user;

import lombok.NonNull;
import ru.tsc.vinokurov.tm.enumerated.Role;
import ru.tsc.vinokurov.tm.exception.entity.UserNotFoundException;
import ru.tsc.vinokurov.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @NonNull
    public static final String NAME = "user-view-profile";

    @NonNull
    public static final String DESCRIPTION = "View user profile.";

    public static final String ARGUMENT = null;

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        final User user = getAuthService().getUser();
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("E-MAIL: " + user.getEmail());
        System.out.println("ROLE: " + Role.toName(user.getRole()));
    }

    @NonNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
