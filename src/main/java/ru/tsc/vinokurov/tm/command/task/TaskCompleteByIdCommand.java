package ru.tsc.vinokurov.tm.command.task;

import lombok.NonNull;
import ru.tsc.vinokurov.tm.enumerated.Status;
import ru.tsc.vinokurov.tm.util.TerminalUtil;

public final class TaskCompleteByIdCommand extends AbstractTaskCommand {

    @NonNull
    public static final String NAME = "task-complete-by-id";

    @NonNull
    public static final String DESCRIPTION = "Complete task by id.";

    public static final String ARGUMENT = null;

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("ENTER ID:");
        @NonNull final String id = TerminalUtil.nextLine();
        @NonNull final String userId = getUserId();
        getTaskService().changeStatusById(userId, id, Status.COMPLETED);
    }

}
