package ru.tsc.vinokurov.tm.command.system;

import lombok.NonNull;
import ru.tsc.vinokurov.tm.command.AbstractCommand;

import java.util.Collection;

public final class ArgumentsCommand extends AbstractSystemCommand {

    @NonNull
    public static final String NAME = "arguments";

    @NonNull
    public static final String DESCRIPTION = "Show argument list.";

    @NonNull
    public static final String ARGUMENT = "-arg";

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NonNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        @NonNull final Collection<AbstractCommand> commands = getCommandService().getArgumentCommands();
        commands.stream()
                .map(AbstractCommand::getArgument)
                .forEach(System.out::println);
    }

}
