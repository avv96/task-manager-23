package ru.tsc.vinokurov.tm.model;


import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.vinokurov.tm.enumerated.Role;

@Getter
@Setter
public final class User extends AbstractModel {
    @Nullable
    private String login;
    @Nullable
    private String passwordHash;
    @Nullable
    private String email;
    @Nullable
    private String firstName;
    @Nullable
    private String middleName;
    @Nullable
    private String lastName;
    @NonNull
    private Role role = Role.USUAL;

    private boolean locked = false;

}
