package ru.tsc.vinokurov.tm.model;

import org.jetbrains.annotations.Nullable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import ru.tsc.vinokurov.tm.api.model.IWBS;
import ru.tsc.vinokurov.tm.enumerated.Status;
import ru.tsc.vinokurov.tm.util.DateUtil;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class Project extends AbstractUserOwnedModel implements IWBS {
    @NonNull
    private String name = "";
    @NonNull
    private String description = "";
    @NonNull
    private Status status = Status.NOT_STARTED;
    @NonNull
    private Date created = new Date();
    @Nullable
    private Date dateBegin;
    @Nullable
    private Date dateEnd;

    public Project(@NonNull String name, @NonNull Status status, @NonNull Date dateBegin) {
        this.name = name;
        this.status = status;
        this.dateBegin = dateBegin;
    }

    @Override
    public String toString() {
        return name + " : " + DateUtil.toString(dateBegin) + ":" + description + " : " + status.getDisplayName();
    }

}
