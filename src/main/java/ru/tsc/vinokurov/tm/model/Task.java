package ru.tsc.vinokurov.tm.model;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.vinokurov.tm.api.model.IWBS;
import ru.tsc.vinokurov.tm.enumerated.Status;
import ru.tsc.vinokurov.tm.util.DateUtil;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Task extends AbstractUserOwnedModel implements IWBS {
    @NonNull
    private String name = "";
    @NonNull
    private String description = "";
    @NonNull
    private Status status = Status.NOT_STARTED;
    @Nullable
    private String projectId;
    @NonNull
    private Date created = new Date();
    @Nullable
    private Date dateBegin;
    @Nullable
    private Date dateEnd;

    public Task(@NonNull final String name, @NonNull final Status status, @NonNull final Date dateBegin) {
        this.name = name;
        this.status = status;
        this.dateBegin = dateBegin;
    }

    @NotNull
    @NonNull
    @Override
    public String toString() {
        return name + " : " + DateUtil.toString(dateBegin) + ":" + description + " : " + status.getDisplayName();
    }

}
