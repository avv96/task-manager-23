package ru.tsc.vinokurov.tm.model;


import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class AbstractModel {
    @NonNull
    private String id = UUID.randomUUID().toString();

}
