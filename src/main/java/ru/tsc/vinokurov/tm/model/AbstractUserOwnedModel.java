package ru.tsc.vinokurov.tm.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AbstractUserOwnedModel extends AbstractModel {

    private String userId;

}
