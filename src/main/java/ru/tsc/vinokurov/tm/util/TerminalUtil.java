package ru.tsc.vinokurov.tm.util;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;

import java.util.Date;
import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    @NonNull
    static Date nextDate() {
        return DateUtil.toDate(SCANNER.nextLine());
    }

    @NonNull
    static String nextLine() {
        return SCANNER.nextLine();
    }

    @NotNull
    @NonNull
    static Integer nextNumber() {
        final String value = nextLine();
        return Integer.parseInt(value);
    }
}
