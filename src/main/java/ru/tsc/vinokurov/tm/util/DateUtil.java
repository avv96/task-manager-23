package ru.tsc.vinokurov.tm.util;

import lombok.NonNull;
import org.jetbrains.annotations.Nullable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public interface DateUtil {
    @NonNull
    String PATTERN = "yyyy.MM.dd";
    @NonNull
    SimpleDateFormat FORMATTER = new SimpleDateFormat(PATTERN);

    static Date toDate(@NonNull final String value) {
        try {
            return FORMATTER.parse(value);
        } catch (ParseException e) {
            System.err.println(e.getMessage());
            return null;
        }
    }

    static String toString(@Nullable final Date value) {
        if (value == null) return "";
        return FORMATTER.format(value);
    }
}
