package ru.tsc.vinokurov.tm.exception.system;

import ru.tsc.vinokurov.tm.exception.AbstractException;

public final class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error! Access denied. Login or password is incorrect...");
    }

}
