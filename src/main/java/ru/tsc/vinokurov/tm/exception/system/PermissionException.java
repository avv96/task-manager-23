package ru.tsc.vinokurov.tm.exception.system;

import ru.tsc.vinokurov.tm.exception.AbstractException;

public final class PermissionException extends AbstractException {

    public PermissionException() {
        super("Error! Insufficient rights to execute command...");
    }

}
