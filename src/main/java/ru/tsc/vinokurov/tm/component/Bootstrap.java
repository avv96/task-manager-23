package ru.tsc.vinokurov.tm.component;

import lombok.Getter;
import lombok.NonNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.vinokurov.tm.api.repository.ICommandRepository;
import ru.tsc.vinokurov.tm.api.repository.IProjectRepository;
import ru.tsc.vinokurov.tm.api.repository.ITaskRepository;
import ru.tsc.vinokurov.tm.api.repository.IUserRepository;
import ru.tsc.vinokurov.tm.api.service.*;
import ru.tsc.vinokurov.tm.command.AbstractCommand;
import ru.tsc.vinokurov.tm.command.binding.BindTaskToProject;
import ru.tsc.vinokurov.tm.command.binding.UnbindTaskFromProject;
import ru.tsc.vinokurov.tm.command.project.*;
import ru.tsc.vinokurov.tm.command.system.*;
import ru.tsc.vinokurov.tm.command.task.*;
import ru.tsc.vinokurov.tm.command.user.*;
import ru.tsc.vinokurov.tm.enumerated.Role;
import ru.tsc.vinokurov.tm.enumerated.Status;
import ru.tsc.vinokurov.tm.exception.system.ArgumentNotSupportedException;
import ru.tsc.vinokurov.tm.exception.system.CommandNotSupportedException;
import ru.tsc.vinokurov.tm.model.Project;
import ru.tsc.vinokurov.tm.model.Task;
import ru.tsc.vinokurov.tm.repository.CommandRepository;
import ru.tsc.vinokurov.tm.repository.ProjectRepository;
import ru.tsc.vinokurov.tm.repository.TaskRepository;
import ru.tsc.vinokurov.tm.repository.UserRepository;
import ru.tsc.vinokurov.tm.service.*;
import ru.tsc.vinokurov.tm.util.DateUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@Getter
public final class Bootstrap implements IServiceLocator {
    @NonNull
    private final ICommandRepository commandRepository = new CommandRepository();
    @Getter
    private final ICommandService commandService = new CommandService(commandRepository);
    @NonNull
    private final ITaskRepository taskRepository = new TaskRepository();
    @NonNull
    private final ITaskService taskService = new TaskService(taskRepository);
    @NonNull
    private final IProjectRepository projectRepository = new ProjectRepository();
    @NonNull
    private final IProjectService projectService = new ProjectService(projectRepository);
    @NonNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
    @NonNull
    private final ILoggerService loggerService = new LoggerService();
    @NonNull
    private final IUserRepository userRepository = new UserRepository();
    @NonNull
    private final IUserService userService = new UserService(userRepository, taskRepository, projectRepository);
    @NonNull
    private final IAuthService authService = new AuthService(userService);

    {
        registerCommand(new InfoCommand());
        registerCommand(new VersionCommand());
        registerCommand(new HelpCommand());
        registerCommand(new AboutCommand());
        registerCommand(new ArgumentsCommand());
        registerCommand(new CommandsCommand());
        registerCommand(new TaskListCommand());
        registerCommand(new TaskChangeStatusByIdCommand());
        registerCommand(new TaskChangeStatusByIndexCommand());
        registerCommand(new TaskClearCommand());
        registerCommand(new TaskCompleteByIdCommand());
        registerCommand(new TaskCompleteByIndexCommand());
        registerCommand(new TaskCreateCommand());
        registerCommand(new TaskRemoveByIdCommand());
        registerCommand(new TaskRemoveByIndexCommand());
        registerCommand(new TaskShowByIdCommand());
        registerCommand(new TaskShowByIndexCommand());
        registerCommand(new TaskStartByIdCommand());
        registerCommand(new TaskStartByIndexCommand());
        registerCommand(new TaskUpdateByIdCommand());
        registerCommand(new TaskUpdateByIndexCommand());
        registerCommand(new ProjectListCommand());
        registerCommand(new ProjectChangeStatusByIdCommand());
        registerCommand(new ProjectChangeStatusByIndexCommand());
        registerCommand(new ProjectClearCommand());
        registerCommand(new ProjectCompleteByIdCommand());
        registerCommand(new ProjectCompleteByIndexCommand());
        registerCommand(new ProjectCreateCommand());
        registerCommand(new ProjectRemoveByIdCommand());
        registerCommand(new ProjectRemoveByIndexCommand());
        registerCommand(new ProjectShowByIdCommand());
        registerCommand(new ProjectShowByIndexCommand());
        registerCommand(new ProjectStartByIdCommand());
        registerCommand(new ProjectStartByIndexCommand());
        registerCommand(new ProjectUpdateByIdCommand());
        registerCommand(new ProjectUpdateByIndexCommand());
        registerCommand(new BindTaskToProject());
        registerCommand(new UnbindTaskFromProject());
        registerCommand(new UserChangePasswordCommand());
        registerCommand(new UserLoginCommand());
        registerCommand(new UserLogoutCommand());
        registerCommand(new UserRegisterCommand());
        registerCommand(new UserUpdateProfileCommand());
        registerCommand(new UserViewProfileCommand());
        registerCommand(new UserLockCommand());
        registerCommand(new UserUnlockCommand());
        registerCommand(new UserRemoveCommand());
        registerCommand(new ExitCommand());
    }

    public void close() {
        System.exit(0);
    }

    public void processCommand(@Nullable final String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException();
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void initUsers() {
        userService.create("user", "user");
        userService.create("test", "test", "test@example.com");
        userService.create("admin", "admin", "admin@example.com", Role.ADMIN);
    }

    public void processCommand() {
        try (final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            @Nullable String command;
            System.out.print("Enter command: ");
            while ((command = reader.readLine()) != null) {
                try {
                    loggerService.command(command);
                    processCommand(command);
                    System.out.println("[OK]");
                } catch (final Exception e) {
                    loggerService.error(e);
                    System.err.println("[FAIL]");
                }
                System.out.print("Enter command: ");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void processArgument(@Nullable final String arg) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(arg);
        abstractCommand.execute();
    }

    public boolean processArgument(@Nullable final String[] args) {
        if (args == null || args.length < 1) return false;
        for (String arg : args) {
            try {
                processArgument(arg);
                System.out.println("[OK]");
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.err.println("[FAIL]");
            }
        }
        return true;
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(() -> loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **")));
    }

    public void run(final String[] args) {
        if (processArgument(args)) close();
        initLogger();
        initUsers();
        processCommand();
    }

    private void registerCommand(@Nullable final AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

}
